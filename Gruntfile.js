"use strict";

const grunt = require("grunt");
grunt.loadNpmTasks("grunt-aws-lambda");

grunt.initConfig({
    lambda_invoke: {default: {}},
    lambda_deploy: {
        default: {
            arn: "arn:aws:lambda:us-east-2:892572355854:function:Resizer",
            options: {
                region: "us-east-2",
                timeout: 5,
                memory: 128
            }
        }
    },
    lambda_package: {default: {}}
});

grunt.registerTask("deploy", [
    "lambda_package",
    "lambda_deploy"
]);

grunt.registerTask("invoke", "lambda_invoke");