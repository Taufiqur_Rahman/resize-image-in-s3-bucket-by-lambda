# Create lambda function by invoking S3 bucket locally

### Description:
***
Creating a lambda function locally by node.js and push into the AWS lambda console. In the S3 bucket,create a **source-bucket** and **destination bucket** (e.g. image-bucket , image-bucket-resize). Upload a jpg in source-bucket,which will be resized by lambda function and store in the destination bucket.

### Prerequisites:
***
* Environment: Windows.

* [Node.js](https://nodejs.org/en/) version: v8.11.1 or latest.

* NPM version: latest version. If not install,then-
```
$ npm i latest-version
```

* [Grunt.js](https://gruntjs.com/getting-started) version: v1.0.4 or latest 

```
$ npm install -g grunt-cli
```
* GM npm version: GraphicsMagick and ImageMagick for node (v1.23.1)
```
$ npm install gm
```

### To-do:
***
* Upload a jpg in S3 source bucket.
* Create a lambda function into lambda console.

### Configuration:
***
* Upload a jpg into s3 source bucket.

* Now it need to install npm inside the folder **$npm install**. A node_modules folder will be auto generated which will contain few javascript libraries files. 

* Create **index.js** which is the main lambda handler function.

* Create **GruntFile.js** file. The file will contain arn for lambda function. Create a lambda function into the console, after that inside the lambda function top-right conrner arn will be found.

* Download or make an **event.json**. Here json file contains s3 source bucket details. (e.g.
```
"bucket": {
            "arn": "arn:aws:s3:::avro-bucket",
            "name": "avro-bucket",
            "ownerIdentity": {
              "principalId": "EXAMPLE"
            }
```            
)
"arn" will be the source bucket arn.

### Run and invoke lambda function:
***
open powershell and write following lines:

* Locally testing the lambda function and save dev-dependency. 
```
$ npm install grunt grunt-aws-lambda --save-dev
```

* Check all installed dependencies from package.json.

* The following command calls a deploy functionality, which is specified in gruntfile.js and push the lambda function

```
$ grunt deploy
```

* Finally, it need to push the lambda into console. The following command generally push the lambda_function into aws lambda console. However, it mainly helps to execute the lambda_function locally.
```
$ grunt invoke
```

Now go to AWS S3 console and check the destination bucket. A resize-picture will be stored.

### Additional configuration:
***

In few cases, aws credentials have to re-configured. 

* First it is important to create new user in IAM console. Go to [IAM Console](https://console.aws.amazon.com/iam/home?region=us-east-2#/users), then in the navigation pane choose "User".

* Choose own IAM user name.

* From "Security Credentials", get the 'Access Key Id' and 'Secret access key'.

* Open the terminal on local mechine.

* Paste the following line
```
$ aws configure
```
* Then paste the 'Access key Id' and 'Secret access key'. Example:
```
$ aws configure
AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE
AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
Default region name [None]: us-west-2
Default output format [None]: json
```
**The same way own user credentials could be re-changed** 



